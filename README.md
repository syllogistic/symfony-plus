Symfony Plus
=====

Have you ever spent hours configuring bundles so they all work together nicely?

Symfony Plus saves you that trouble, but providing a pre-configured environment with some of the
most common bundles. It consists of:

* Symfony Standard Edition - https://github.com/symfony/symfony-standard

PLUS:

* FOSUserBundle - https://github.com/FriendsOfSymfony/FOSUserBundle
* HWIOAuthBundle - https://github.com/hwi/HWIOAuthBundle
* SonataAdminBundle - http://sonata-project.org/bundles/admin/2-2/doc/index.html

The bundles are pre-configured to give you:

* Basic user registration / login using email address as the primary identifier
* Integrated login with Google or Facebook (add other providers with additional configuration)
* Pre-configured UI for to edit and configure users

Installation
-----

1. Fork or download symfony-plus
1. Create a new MySQL database
1. Install composer.phar from https://getcomposer.org/download/
1. Get the /vendor source: `php composer.phar install`
    * Set parameters for your database
    * Set parameters for social login (you can skip this step and set up social login later)
1. Create the initial database schema: `php app/console doctrine:schema:update --force` 
1. Download PHPUnit from https://phar.phpunit.de/phpunit.phar
1. Run tests: `php phpunit.phar -c app/`
1. Browse to http://localhost/your-app/app_dev.php/ and wait for the Symfony debug toolbar to load

User Accounts
-----

1. To register a new account, browse to /register
    * NOTE: The default FOSUserBundle templates are very basic and have no styling 
1. Once registered and logged in, browse to /admin, then User List
1. Click the user you created, then Roles, then add the Admin role, Update and close
1. Now you can secure the /admin by editing app/config/security and changing the role to ROLE_ADMIN:
    * `- { path: ^/admin/, role: ROLE_ADMIN }`

Social Login
-----
1. To test social login locally, you'll need a "fake" domain
    * We recommend setting up http://dev.yourproject.com using an Apache virtual host
    * Then you can map your local IP address to this domain in your hosts file
1. Create your Google client id and secret at https://console.developers.google.com
1. Create your Facebook client id and secret at https://developers.facebook.com/
1. Browse to /connect and choose Google or Facebook
