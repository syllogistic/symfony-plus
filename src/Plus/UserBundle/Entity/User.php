<?php
namespace Plus\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) */
    protected $facebook_id;
 
    // Hack for hwi\oauth-bundle\HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider.php at line 93
    public function setFacebook_id($facebookId) {return $this->setFacebookId($facebookId);}

    /** @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) */
    protected $facebook_access_token;
    
    /** @ORM\Column(name="google_id", type="string", length=255, nullable=true) */
    protected $google_id;
    
    // Hack for hwi\oauth-bundle\HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider.php at line 93
    public function setGoogle_id($googleId) {return $this->setGoogleId($googleId);}

    /** @ORM\Column(name="full_name", type="string", length=255, nullable=true) */
    protected $full_name;
    
    /** @ORM\Column(name="picture_url", type="string", length=1023, nullable=true) */
    protected $picture_url;
    
    

    
        
    public function __construct()
    {
        parent::__construct();
        
        // Custom construction code goes here...
    }
    

// override methods for username and tie them with email field

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }
    
    
    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set facebook_id
     *
     * @param string $facebookId
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebook_id
     *
     * @return string 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebook_access_token
     *
     * @param string $facebookAccessToken
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebook_access_token
     *
     * @return string 
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set google_id
     *
     * @param string $googleId
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get google_id
     *
     * @return string 
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set full_name
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName)
    {
        $this->full_name = $fullName;

        return $this;
    }

    /**
     * Get full_name
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->full_name;
    }

    /**
     * Set picture_url
     *
     * @param string $pictureUrl
     * @return User
     */
    public function setPictureUrl($pictureUrl)
    {
        $this->picture_url = $pictureUrl;

        return $this;
    }

    /**
     * Get picture_url
     *
     * @return string 
     */
    public function getPictureUrl()
    {
        return $this->picture_url;
    }


}
