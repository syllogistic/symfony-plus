<?php
// Pre-deployment tasks
namespace Plus\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('plus:generate')
            ->setDescription('Generate entity functions and update schema')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
            $output->writeln("Generating entity functions and updating schema");

            // Generate Entity functions
            system('php app/console doctrine:generate:entities PlusUserBundle');

            // Update database schema
            system('php app/console doctrine:schema:update --force');

            // Run tests
            system('php phpunit.phar -c app/');
    }
}